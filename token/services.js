const {
    JwtPayload
} = require('../security/jwt/models.js');

const {
    generateToken
} = require('../security/jwt/token.js');

const {
    query
} = require('../data');

const {
    ServerError
} = require('../errors');

const refresh = async (id) => {
    console.info(`Refreshing token for user ${id}`);

    const token = await generateToken(new JwtPayload(id));

    return token;
};

const getRole = async (id) => {
    console.info(`Getting role and email for user ${id}`);

    const users = await query('SELECT email, isadmin FROM users WHERE id = $1', [id]);

    if (users.length === 0) {
        throw new ServerError('The current user exists no more!', 401);
    }

    const role = users[0].isadmin ? process.env.ADMIN_ROLE : process.env.READER_ROLE;
    const email = users[0].email;

    return {
        role,
        email
    };
}

module.exports = {
    refresh,
    getRole
}