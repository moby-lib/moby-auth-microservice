const Router = require('express').Router();

const {
    refresh,
    getRole
} = require('./services.js');

const {
    authorizeAndExtractToken,
    authorizeAndExtractTokenNoExpire
} = require('../security/jwt/filters.js');

Router.get('/refresh', authorizeAndExtractTokenNoExpire, async (req, res) => {
    const {
        userId
    } = req.state.decoded;

    const token = await refresh(userId);

    res.json({
        token
    });
});

Router.get('/authorizeAndReturnDetails', authorizeAndExtractToken, async (req, res) => {
    const {
        userId
    } = req.state.decoded;

    const {
        role,
        email
     } = await getRole(userId);

    res.json({
        userId,
        role,
        email
    });
});

module.exports = Router;