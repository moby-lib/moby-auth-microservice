# Moby Auth Microservice

Authentication, Authorization and User Management microservice for the Moby Lib project.

## REST API

|Method|Path|Description|Req Body|Res Body|Minimum Role|
|---|---|---|---|---|---|
| POST | /api/auth/users/ | Registers a user. Returns a jwt token to be used for authentication. | {"email": <your_email>, "password": <your_password>} | {"token": <token_string>} | None |
| POST | /api/auth/users/login | Logs in a user. Returns a jwt token to be used for authentication. | {"email": <your_email>, "password": <your_password>} | {"token": <token_string>} | None |
| DELETE | /api/auth/users/ | Removes the current user, the books added by the user and the subscription of the user. |  |  | READER |
| GET | /api/auth/token/refresh | Refreshes the current token. |  | {"token": <token_string>} | READER |
| GET | /api/auth/token/authorizeAndReturnDetails | Returns the email and role of the user using this token. Used by the other services for authorization. |  | {"userId": <used_userId>, "role": <your_role>, "email": <your_email>} | READER |


## Deployment

### Monorepo configurations - RECOMMENDED

[monorepo](https://gitlab.com/moby-lib/moby-monorepo)

### Using Docker - NOT RECOMMENDED
```
sudo docker network create auth-net
sudo docker run --name moby-auth-microservice --network="auth-net" --env-file .env -p 3001:3001 registry.gitlab.com/moby-lib/moby-auth-microservice:1.0
sudo docker network connect microservices-net moby-auth-microservice

cd database-deploy; sudo docker-compose up
sudo docker network connect auth-net database-deploy_auth-db_1
```