const {
    generateToken
} = require('../security/jwt/token.js');

const {
    JwtPayload
} = require('../security/jwt/models.js');

const {
    query
} = require('../data');

const { hash, compare } = require('../security/passwords');

const { ServerError } = require('../errors');

const {
    sendRequest
} = require('../http-client');

const register = async (email, password) => {
    console.info(`Registering user ${email}`);

    const hashedPassword = await hash(password);

    try {
        const users = await query(`INSERT INTO users (email, password) VALUES ($1, $2) RETURNING id`, [email, hashedPassword]);

        const {
            id
        } = users[0];

        const token = await generateToken(new JwtPayload(id));
        
        return token;
    } catch (e) {
        if (e.code === '23505') {
            throw new ServerError(`Email ${email} already exists!`, 409);
        }
        throw e;
    }
}

const login = async (email, plainTextPassword) => {

    console.info(`Authenticating user ${email}`);

    const users = await query ('SELECT id, password FROM users WHERE email = $1', [email]);

    if (users.length === 0) {
        throw new ServerError(`No user found with this email ${email}`, 404);
    }

    const {
        id, 
        password
    } = users[0];

    const isPasswordOk = await compare(plainTextPassword, password);

    if (!isPasswordOk) {
        throw new ServerError('Bad credentials!', 403);
    }

    const token = await generateToken(new JwtPayload(id));

    return token;

}

const remove = async (userId) => {

    console.info(`Removing user ${userId}...`);
    
    const libraryOptions = {
        url: `http://${process.env.LIBRARY_SERVICE}/api/books/user/${userId}`,
        method: 'DELETE'
    };

    console.info(`Sending delete request for user ${userId} books...`);

    await sendRequest(libraryOptions);

    console.info(`Sending delete request for user ${userId} newsletter...`);

    const newsletterOptions = {
        url:`http://${process.env.NOTIFICATION_SERVICE}/api/newsletter/unsubscribe/user/${userId}`,
        method: 'DELETE'
    };

    await sendRequest(newsletterOptions);

    await query('DELETE FROM users WHERE id = $1', [userId]);
}
module.exports = {
    register,
    login, 
    remove
}